## Video Guide: [youtube](https://youtu.be/eeO7EW5Gcm4)

## Text Guide:

Go to directory with backend Dockerfile

`$ docker build . -t syunsha/backend`
**If you wonna change image name you also should change image name in deploument yaml for backend**

```
$ docker push syunsha/backend

$ minikube start
```

Go to directory with deplyment database files

```
$ kubectl apply -f postgres-config.yaml

$ kubectl apply -f postgres-storage.yaml

$ kubectl apply -f postgres-service.yaml

$ kubectl apply -f postgres-deployment.yaml
```

Go to directory with deplyment backend files

```
$ kubectl apply -f backend-config.yaml

$ kubectl apply -f deployment-todos-backend.yaml

$ kubectl apply -f service-todos-backend.yaml
```

You should get a backend url and paste him in the frontend env file, one way of this

```
$ minikube service todos-backend-lb
```

After that copy url from output.

Go to directory with frontend Dockerfile

```
$ docker build . -t syunsha/frontend

$ docker push syunsha/frontend
```

**If you wonna change image name you also should change image name in deploument yaml for frontend**

Go to directory with deplyment frontend files

```
$ kubectl apply -f deployment-todos-frontend.yaml

$ kubectl apply -f service-todos-frontend.yaml
```

You should get a frontend url and paste him in the browser command line, one way of this

```
$ minikube service todos-frontend-lb
```

and copy frontend url.

This url can be inserted into the search bar of the browser and we will get a working application.
